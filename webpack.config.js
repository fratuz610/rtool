'use strict';
var path = require('path');
var webpack = require('webpack');
var nodeExternals = require('webpack-node-externals');

var fs = require('fs');
var gracefulFs = require('graceful-fs');
gracefulFs.gracefulify(fs);

let projectName = 'redtool';

module.exports = {
    devtool: 'source-map',
    entry: [
      path.join(__dirname, '/src/'+projectName+'.ts')
    ],
    output: {
      path: path.join(__dirname, '/publish/'),
      filename: projectName + '.js'
    },
    target: 'node', // in order to ignore built-in modules like path, fs, etc.
    externals: [nodeExternals({
      modulesFromFile: true
    })],
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
      }),
      new webpack.BannerPlugin({
        banner: '#!/usr/bin/env node' + "\n" + 'require("source-map-support").install();',
        raw: true,
        entryOnly: false
      })
    ],
    resolve: {
      extensions: ['.ts', '.js']
    },
    module: {
      rules: [ // loaders will work with webpack 1 or 2; but will be renamed "rules" in future
        { test: /\.ts$/, loader: 'ts-loader', options: { configFile: 'tsconfig.webpack.json' } }
      ]
    },
    optimization:{
      minimize: false // <---- disables uglify.
    }
};