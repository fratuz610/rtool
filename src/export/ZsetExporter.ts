import * as ioredis from 'ioredis';
import * as fs from 'fs';
import * as util from 'util';

import { WriteResult, ElemSize } from '../model/Model';
import { ArraySplitter } from '../utils/ArraySplitter';
import { BufferedWriter } from '../utils/BufferedWriter';
import { KeyTransformer } from '../utils/KeyTransformer';

export class ZsetExporter {

    constructor(private client:ioredis.Redis, 
        private outStream:BufferedWriter,
        private keyTransformer:KeyTransformer) {

        }

    async exportZsets(zsetKeyList:string[]):Promise<void> {

        let zsetSizeList = await this.batchZsetSize(zsetKeyList);

        let bigZsetList = [];
        let smallZsetList = [];

        for(let zsetKey of zsetSizeList) {
            if(zsetKey.size > 1000)
                bigZsetList.push(zsetKey.key);
            else
                smallZsetList.push(zsetKey.key);
        }

        //console.log("=> Small sets count: %d", smallSetList.length);
        //console.log("=> Big sets count: %d", bigSetList.length);

        await Promise.all([
            this.exportSmallZsetList(smallZsetList),
            this.exportBigZsetList(bigZsetList)
        ])
        
        return Promise.resolve();

    }

    private async batchZsetSize(zsetKeyList:string[]):Promise<ElemSize[]> {

        let pipeline = this.client.pipeline();

        for(let zsetKey of zsetKeyList)
            pipeline.zcard(zsetKey);
        
        let replies = await pipeline.exec();

        let retList:ElemSize[] = [];

        for(let [index, reply] of replies.entries()) {

            if(reply[0])
                return Promise.reject(reply[0]);
            
            retList.push({key:zsetKeyList[index], size: parseInt(reply[1])});
        }

        return Promise.resolve(retList);

    }

    private async exportSmallZsetList(zsetKeyList:string[]):Promise<void> {

        if(zsetKeyList.length === 0)
            return Promise.resolve();

        let pipeline = this.client.pipeline();

        for(let zsetKey of zsetKeyList)
            pipeline.zrange(zsetKey, 0, -1, 'WITHSCORES');
        
        let replies = await pipeline.exec();

        for(let [index, reply] of replies.entries()) {

            if(reply[0])
                return Promise.reject(reply[0]);

            let zsetList = reply[1];

            // we export the set members in group of 50
            this.exportZsetMembers(zsetKeyList[index], zsetList);

        }
        return Promise.resolve();

    }

    private async exportBigZsetList(zsetKeyList:string[]):Promise<void> {
        
        if(zsetKeyList.length === 0)
            return Promise.resolve();
        
        for(let zsetKey of zsetKeyList) {

            let cursor = 0;
            while(true) {

                let scanRes = await this.client.zscan(zsetKey, cursor, 'COUNT', 1000);

                // we export the set members in group of 50
                this.exportZsetMembers(zsetKey, scanRes[1]);

                cursor = parseInt(scanRes[0]);

                if(cursor === 0)
                    break;
                
            }
        }

        return Promise.resolve();

    }

    private exportZsetMembers(key:string, zsetMembers:any[]) {

        // we receive a list of member1 score1 member2 score2 
        // but we need a list of score1 member1 score2 member2
        let workList = [];
        for(let i = 0; i < zsetMembers.length; i+=2) {
            workList.push(zsetMembers[i+1]);
            workList.push(zsetMembers[i]);
        }
    
        
        let arraySplitter = new ArraySplitter(workList, 50);
            
        while(true) {

            let curArray = arraySplitter.nextChunk();

            if(curArray.length === 0)
                break;

            let redisKey = this.keyTransformer.mapKey(key);

            let str = util.format("ZADD %s %s", encodeURI(redisKey), curArray.map(zsetItem => encodeURI(zsetItem)).join(' '));

            this.outStream.write(str + "\r\n");
        }
    }

    


}