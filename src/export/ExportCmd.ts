import { CommandModule, Argv } from "yargs";

import * as IoRedis from 'ioredis';
import * as util from 'util';

import { ScanCountResult, ScanResult, KeyType, ExportConfig } from "../model/Model";
import { ArraySplitter } from "../utils/ArraySplitter";
import { BufferedWriter } from "../utils/BufferedWriter";
import { HashExporter } from "./HashExporter";
import { StringExporter } from "./StringExporter";
import { SetExporter } from "./SetExporter";
import { ListExporter } from "./ListExporter";
import { ZsetExporter } from "./ZsetExporter";
import { KeyTransformer } from "../utils/KeyTransformer";

export class ExportCmd {

    private client:IoRedis.Redis;
    private exportStats:ExportStats;
    private hashExp:HashExporter;
    private stringExp:StringExporter;
    private setExp:SetExporter;
    private listExp:ListExporter;
    private zsetExp:ZsetExporter;
    private keyTransformer:KeyTransformer;

    constructor() {
        this.exportStats = new ExportStats();
    }

    private exportHandler(argv:ExportConfig) {

        console.log(`RTOOL`);
        console.log(`* redis host: ${argv.host}`);
        console.log(`* redis port: ${argv.port}`);
        console.log(`* filter: ${argv.filter}`);
        console.log(`* output to: ${argv.output}`);
        console.log(`* transform: ${argv.transform ? argv.transform : 'n/a'}`);

        this.doExport(argv)
          .then(() => {
              console.log("All done");
              process.exit(0);
          })
          .catch(err => {
              console.log("ERROR: %j", err);
              process.exit(-1);
          });
    }

    private async doExport(argv:ExportConfig):Promise<void> {

        console.log("Connecting to %s:%d", argv.host, argv.port);

        this.client = new IoRedis(argv.port, argv.host, { lazyConnect: true });
        this.client.on('error', (err:Error) => { 
            console.log("Redis error: %s -> %s", err.name, err.message) 
            this.client.quit();
        });

        // we connect
        await this.client.connect();

        console.log("...connected");

        // we remove the ending asterixes 
        argv.filter = argv.filter.replace(/\*+$/g, '');
        argv.transform = argv.transform ? argv.transform.replace(/\*+$/g, '') : undefined;

        // plumbing
        
        let writeStream = new BufferedWriter(argv.output);
        this.keyTransformer = new KeyTransformer(argv.filter, argv.transform);
        this.hashExp = new HashExporter(this.client, writeStream, this.keyTransformer);
        this.listExp = new ListExporter(this.client, writeStream, this.keyTransformer);
        this.setExp = new SetExporter(this.client, writeStream, this.keyTransformer);
        this.stringExp = new StringExporter(this.client, writeStream, this.keyTransformer);
        this.zsetExp = new ZsetExporter(this.client, writeStream, this.keyTransformer);

        // we kick off the filtering
        let result = await this.scanCount(argv.filter);

        console.log("Pattern `%s` has %d keys", result.pattern, result.numKeys);

        if(result.numKeys === 0) {
            console.log("** NO keys matching pattern `%s`. Nothing to do.", result.pattern);
            return Promise.resolve();
        }

        for(let key of result.keyList)
            console.log("** %s", key);

        console.log("** ...");

        let cursor = 0;
        let iterationNum = 0;
        let totalKeys = 0;

        while(true) {

          let singleScanRes = await this.scanIter(argv.filter, cursor);
          
          totalKeys += singleScanRes.keyList.length;

          let typeList = await this.bulkType(singleScanRes.keyList);

          // we export a delete statement for each key we are exporting so that we can import safely
          await this.exportDeleteKeyList(typeList.map(type => type.key), writeStream);

          // we export
          await this.batchExport(typeList, writeStream);
          
          iterationNum++;
          cursor = singleScanRes.nextCursor;

          if(iterationNum % 10 === 0)
            console.log("Iteration: %d / total keys: %d", iterationNum, totalKeys);

          if(cursor === 0) {
            console.log("Finished after %d iterations / total keys: %d", iterationNum, totalKeys);
            break;
          }
        }
        
        console.log("=> Strings exported: %d", this.exportStats.stringCount);
        console.log("=> Hashes exported: %d", this.exportStats.hashCount);
        console.log("=> Sets exported: %d", this.exportStats.setCount);
        console.log("=> Lists exported: %d", this.exportStats.listCount);
        console.log("=> ZSets exported: %d", this.exportStats.zsetCount);

        // we close the stream
        writeStream.close();

        console.log("File written %s", argv.output);

        // we disconnect
        await this.client.disconnect();

        return Promise.resolve();
    }

    private async scanCount(pattern:string):Promise<ScanCountResult> {

        let cursor = 0;
        
        let scanResult = {
            pattern:pattern,
            numKeys: 0,
            keyList: []
        }

        do {

            let res = await this.client.scan(cursor, 'MATCH', pattern + '*', 'COUNT', 1000);

            cursor = parseInt(res[0]);
            let curKeyList = res[1];

            scanResult.numKeys+= curKeyList.length;

            if(scanResult.keyList.length < 10)
                scanResult.keyList = scanResult.keyList.concat(curKeyList);

        } while(cursor !== 0);

        // we return only a small sample of the keys
        scanResult.keyList = scanResult.keyList.slice(0, 10);

        return Promise.resolve(scanResult);
    }

    private async scanIter(pattern:string, cursor:number):Promise<ScanResult> {

        let res = await this.client.scan(cursor, 'MATCH', pattern + '*', 'COUNT', 1000);

        let scanResult = {
            pattern: pattern,
            nextCursor: parseInt(res[0]),
            keyList: res[1]
        }
        
        return Promise.resolve(scanResult);
    }

    private async bulkType(keyList:string[]):Promise<KeyType[]> {

      let pipeline = this.client.pipeline();

      for(let key of keyList)
        pipeline.type(key);

      let replies = await pipeline.exec();

      let retList = [];

      for(let [index, reply] of replies.entries()) {

        if(reply[0])
            return Promise.reject(reply[0]);
        
        retList.push({key:keyList[index], type:reply[1]});
      }
      
      return Promise.resolve(retList);
    }

    private async batchExport(keyTypeList:KeyType[], outStream:BufferedWriter):Promise<void> {

        let stringKeyList = [];
        let hashKeyList = [];
        let listKeyList = [];
        let setKeyList = [];
        let zsetKeyList = [];

        for(let keyType of keyTypeList) {
            switch(keyType.type) {
                case 'string': stringKeyList.push(keyType.key); break;
                case 'hash': hashKeyList.push(keyType.key); break;
                case 'set': setKeyList.push(keyType.key); break;
                case 'list': listKeyList.push(keyType.key); break;
                case 'zset': zsetKeyList.push(keyType.key); break;
            }
        }

        this.exportStats.stringCount += stringKeyList.length;
        this.exportStats.hashCount += hashKeyList.length;
        this.exportStats.setCount += setKeyList.length;
        this.exportStats.listCount += listKeyList.length;
        this.exportStats.zsetCount += zsetKeyList.length;

        if(stringKeyList.length > 0) 
          await this.stringExp.exportStrings(stringKeyList);

        if(hashKeyList.length > 0) 
          await this.hashExp.exportHashes(hashKeyList);

        if(setKeyList.length > 0) 
          await this.setExp.exportSets(setKeyList);

        if(listKeyList.length > 0) 
          await this.listExp.exportLists(listKeyList);

        if(zsetKeyList.length > 0)
          await this.zsetExp.exportZsets(zsetKeyList);

        return Promise.resolve();
    }

    private async exportDeleteKeyList(keyList:string[], outStream:BufferedWriter) {

      let arraySplitter = new ArraySplitter(keyList, 50);
          
      while(true) {

        let curArray = arraySplitter.nextChunk();

        if(curArray.length === 0)
            break;

        let transfList = curArray.map(redisKey => encodeURI(this.keyTransformer.mapKey(redisKey)));

        let str = util.format("DEL %s", transfList.join(' '));
        await outStream.write(str + "\r\n");
      }

    }


    public commandModule:CommandModule = {
        command: 'export <filter> <output>',
        aliases: 'e',
        describe: 'Exports a subset of a redis keyspace to a file',
        builder: {
            'filter': {
                alias: 'f',
                type: 'string' as 'string',
                demand: 'Please specify a redis key filter',
                nargs: 1,
                describe: 'Specifies a keyspace filter'
            },
            'output': {
                alias: 'o',
                type: 'string' as 'string',
                demand: 'Please specify a output file',
                nargs: 1,
                describe: 'Specifies a output file',
            },
            'transform': {
                alias: 't',
                type: 'string' as 'string',
                nargs: 0,
                describe: 'Transforms each key upon export',
            }
        },
        handler: (argv) => { this.exportHandler(argv); }
    }

}

class ExportStats {
    stringCount:number = 0;
    hashCount:number = 0;
    setCount:number = 0;
    listCount:number = 0;
    zsetCount:number = 0;
}
