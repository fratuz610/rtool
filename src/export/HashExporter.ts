import * as IoRedis from 'ioredis';
import * as fs from 'fs';
import * as util from 'util';

import { ElemSize, WriteResult } from '../model/Model';
import { BufferedWriter } from '../utils/BufferedWriter';
import { KeyTransformer } from '../utils/KeyTransformer';

export class HashExporter {

    constructor(private client:IoRedis.Redis, 
        private outStream:BufferedWriter,
        private keyTransformer:KeyTransformer) {

        }

    async exportHashes(hashKeyList:string[]):Promise<void> {

        // we get the size of each hash
        let sizeList = await this.batchSize(hashKeyList);

        let smallHashList = [];
        let bigHashList = [];

        for(let keySize of sizeList) {
          if(keySize.size < 100)
            smallHashList.push(keySize.key);
          else
            bigHashList.push(keySize.key);
        }

        let resList = await Promise.all([
            this.exportSmallHashList(smallHashList), 
            this.exportBigHashList(bigHashList), 
        ])

        return Promise.resolve();
    }

    private async batchSize(hashKeyList:string[]):Promise<ElemSize[]> {

      // first we inspect how big are the hashes
      let inspectPipeline = this.client.pipeline();

      for(let hashKey of hashKeyList)
          inspectPipeline.hlen(hashKey);

      let sizeList:ElemSize[] = [];
      let replies = await inspectPipeline.exec();

      for(let [index, reply] of replies.entries()) {

        if(reply[0])
          return Promise.reject(reply[0]);
        sizeList.push({key:hashKeyList[index], size:parseInt(reply[1])});

      }
      return Promise.resolve(sizeList);
    }

    private async exportSmallHashList(hashKeyList:string[]):Promise<void> {
        
      let pipeline = this.client.pipeline();

      for(let hashKey of hashKeyList)
        pipeline.hgetall(hashKey);
      
      let replies = await pipeline.exec();

      for(let [index, reply] of replies.entries()) {

        if(reply[0])
          return Promise.reject(reply[0]);

        let hash = reply[1];

        for(let hashKey in hash) {
            
          let redisKey = this.keyTransformer.mapKey(hashKeyList[index]);

          // there is no bulk hset unfortunately
          let str = util.format("HSET %s %s %s", encodeURI(redisKey), encodeURI(hashKey), encodeURI(hash[hashKey]));
          this.outStream.write(str + "\r\n");
        }

      }
      return Promise.resolve();
    }

    private async exportBigHashList(hashKeyList:string[]):Promise<void> {
      return this.exportSmallHashList(hashKeyList);
    }


}