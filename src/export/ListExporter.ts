import * as ioredis from 'ioredis';
import * as fs from 'fs';
import * as util from 'util';

import { WriteResult, ElemSize } from '../model/Model';
import { ArraySplitter } from '../utils/ArraySplitter';
import { BufferedWriter } from '../utils/BufferedWriter';
import { KeyTransformer } from '../utils/KeyTransformer';

export class ListExporter {

    constructor(private client:ioredis.Redis, 
        private outStream:BufferedWriter,
        private keyTransformer:KeyTransformer) {

        }
    
    private async batchListSize(listKeyList:string[]):Promise<ElemSize[]> {

        let pipeline = this.client.pipeline();

        for(let listKey of listKeyList)
            pipeline.llen(listKey);
        
        let replies = await pipeline.exec();

        let retList:ElemSize[] = [];

        for(let [index, reply] of replies.entries()) {

            if(reply[0])
                return Promise.reject(reply[0]);
            
            retList.push({key:listKeyList[index], size: parseInt(reply[1])});
        }

        return Promise.resolve(retList);
    }

    async exportLists(listKeyList:string[]):Promise<void> {

        let batchListSize = await this.batchListSize(listKeyList);

        let smallListList = [];
        let bigListList = [];

        for(let listSize of batchListSize) {
            if(listSize.size > 1000)
                bigListList.push(listSize.key);
            else
                smallListList.push(listSize.key);
        }

        //console.log("=> Small list count: %d", smallListList.length);
        //console.log("=> Big list count: %d", bigListList.length);

        let resList = await Promise.all([
            this.exportBigLists(bigListList),
            this.exportSmallLists(smallListList)
        ]);
        
        return Promise.resolve();
    }

    private async exportBigLists(listKeyList:string[]):Promise<void> {

        if(listKeyList.length === 0)
            return Promise.resolve();

        // big lists are exported 1 key at a time / 10k items at a time
        const batchSize = 10000;

        for(let listKey of listKeyList) {

            let iteration = 0;

            while(true) {

                let listContent = await this.client.lrange(listKey, iteration * batchSize, (iteration * batchSize) + batchSize-1);

                if(listContent.length === 0)
                    break;
                
                this.exportListMembers(listKey, listContent);

                iteration++;
            }
        }
        
        return Promise.resolve();
    }

    private async exportSmallLists(listKeyList:string[]):Promise<void> {

        if(listKeyList.length === 0)
            return Promise.resolve();
        
        let pipeline = this.client.pipeline();

        for(let listKey of listKeyList)
            pipeline.lrange(listKey, 0, -1);
        
        let replies = await pipeline.exec();

        for(let [index, reply] of replies.entries()) {

            if(reply[0])
                return Promise.reject(reply[0]);

            let listContent = reply[1];

            // we export the list members in group of 50
            this.exportListMembers(listKeyList[index], listContent);

        }
        return Promise.resolve();
        
    }

    private exportListMembers(key:string, list:any[]) {
        
        let arraySplitter = new ArraySplitter(list, 50);
            
        while(true) {

            let curArray = arraySplitter.nextChunk();

            if(curArray.length === 0)
                break;

            let redisKey = this.keyTransformer.mapKey(key);

            let str = util.format("RPUSH %s %s", encodeURI(redisKey), curArray.map(item => encodeURI(item)).join(' '));
            this.outStream.write(str + "\r\n");
        }
    }
}