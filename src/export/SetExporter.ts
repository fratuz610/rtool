import * as ioredis from 'ioredis';
import * as fs from 'fs';
import * as util from 'util';

import { WriteResult, ElemSize } from '../model/Model';
import { ArraySplitter } from '../utils/ArraySplitter';
import { BufferedWriter } from '../utils/BufferedWriter';
import { KeyTransformer } from '../utils/KeyTransformer';

export class SetExporter {

    constructor(private client:ioredis.Redis, 
        private outStream:BufferedWriter,
        private keyTransformer:KeyTransformer) {

        }

    async exportSets(setKeyList:string[]):Promise<void> {

        let setListSize = await this.batchSetSize(setKeyList);

        let smallSetList = [];
        let bigSetList = [];

        for(let [index, elemSize] of setListSize.entries()) {
            if(elemSize.size > 1000)
                bigSetList.push(setKeyList[index]);
            else
                smallSetList.push(setKeyList[index]);
        }

        //console.log("=> Small sets count: %d", smallSetList.length);
        //console.log("=> Big sets count: %d", bigSetList.length);

        await Promise.all([
            this.exportSmallSetList(smallSetList),
            this.exportBigSetList(bigSetList)
        ])
        
        return Promise.resolve();
    }

    private async batchSetSize(setKeyList:string[]):Promise<ElemSize[]> {
        
        let pipeline = this.client.pipeline();

        for(let setKey of setKeyList)
            pipeline.scard(setKey);
        
        let replies = await pipeline.exec();

        let retList:ElemSize[] = [];

        for(let [index, reply] of replies.entries()) {

            if(reply[0])
                return Promise.reject(reply[0]);
            
            retList.push({key:setKeyList[index], size: parseInt(reply[1])});
        }

        return Promise.resolve(retList);
    }

    private async exportSmallSetList(setKeyList:string[]):Promise<void> {

        if(setKeyList.length === 0)
            return Promise.resolve();

        let pipeline = this.client.pipeline();

        for(let setKey of setKeyList)
            pipeline.smembers(setKey);

        let replies = await pipeline.exec();

        for(let [index, reply] of replies.entries()) {

            if(reply[0])
                return Promise.reject(reply[0]);

            let setList = reply[1];

            // we export the set members in group of 50
            this.exportSetMembers(setKeyList[index], setList);

        }
        return Promise.resolve();
    }

    private async exportBigSetList(setKeyList:string[]):Promise<void> {
        
        if(setKeyList.length === 0)
            return Promise.resolve();

        let wr = new WriteResult();
        
        for(let setKey of setKeyList) {

            let cursor = 0;
            while(true) {

                // @ts-ignore
                let scanRes = await client.sscan(setKey, cursor, 'COUNT', 1000);

                // we export the set members in group of 50
                this.exportSetMembers(setKey, scanRes[1]);

                cursor = parseInt(scanRes[0]);

                if(cursor === 0)
                    break;
                
            }

        }

        return Promise.resolve();
    }

    private exportSetMembers(key:string, memberList:any[]) {

        let arraySplitter = new ArraySplitter(memberList, 50);
            
        while(true) {

            let curArray = arraySplitter.nextChunk();

            if(curArray.length === 0)
                break;

            let redisKey = this.keyTransformer.mapKey(key);

            let str = util.format("SADD %s %s", encodeURI(redisKey), curArray.map(setItem => encodeURI(setItem)).join(' '));
            this.outStream.write(str + "\r\n");
        }
    }
}