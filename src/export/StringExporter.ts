import * as ioredis from 'ioredis';
import * as util from 'util';

import { BufferedWriter } from '../utils/BufferedWriter';
import { KeyTransformer } from '../utils/KeyTransformer';

export class StringExporter {

  constructor(private client:ioredis.Redis, 
      private outStream:BufferedWriter,
      private keyTransformer:KeyTransformer) {

  }

  async exportStrings(stringKeyList:string[]):Promise<void> {

    let pipeline = this.client.pipeline();

    for(let stringKey of stringKeyList)
      pipeline.getBuffer(stringKey);

    let replies = await pipeline.exec();

    for(let [index, reply] of replies.entries()) {

      if(reply[0])
        return Promise.reject(reply[0]);

      let redisKey = this.keyTransformer.mapKey(stringKeyList[index]);

      let str = util.format("SET %s %s", encodeURI(redisKey), encodeURI(reply[1]));
      this.outStream.write(str + "\r\n");

    }
    return Promise.resolve();
  }
}