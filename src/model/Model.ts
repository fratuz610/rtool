export class ScanCountResult {
  pattern: string;
  numKeys: number;
  keyList:string[]
}

export class ScanResult {
  pattern: string;
  nextCursor: number;
  keyList:string[]
}

export class KeyType {
  key:string;
  type:'string'|'hash'|'zset'|'list'|'set';
}

export class WriteResult {
  numEntries:number;
  bytes:number;

  constructor() {
    this.numEntries = 0;
    this.bytes = 0;
  }
}

export class ElemSize {
  key:string;
  size:number;
}

export interface BaseConfig {
  host:string;
  port:number;
}

export interface ExportConfig extends BaseConfig {
  filter:string;
  output:string;
  transform?:string;
}

export interface ImportConfig extends BaseConfig {
  input:string;
  dryRun:boolean;
}

export interface SizeConfig extends BaseConfig {
  path:string;
}