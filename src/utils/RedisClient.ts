"use strict";

import * as util from 'util';
import { LogFactory, Log } from './LogFactory';
import * as IoRedis from 'ioredis';
import { BaseConfig } from '../model/Model';

export class RedisClient {

	private log:Log;
	private config:any;
	
	public client:IoRedis.Redis;
	
	constructor(logFactory:LogFactory, config:BaseConfig) {

		this.log = logFactory.get('RedisClient');
    	this.config = config;

    	// unhandled exceptions
		//IoRedis.Promise.onPossiblyUnhandledRejection((err:PromiseError) => {
		  // you can log the error here.
		  //this.log.error("Command %s with args %j: %s -> %s", err.command.name, err.command.args, err.name, err.message);
		//});

		// we create the client
		this.client = new IoRedis(this.config.redisPort, this.config.redisHost, { retryStrategy: (times:number) => 5000 } );

		this.client.on('error', (err:Error) => { this.log.error("Redis error: %s -> %s", err.name, err.message) });
		this.client.on('ready', () => { 
			this.log.info("Redis connected");

			// redis basic configuration
			this.client.config('set', 'maxmemory', 500 * 1014 * 1024); // 500 mb
			this.client.config('set', 'maxmemory-policy', 'volatile-lru'); // allows for log rotation
		});
		this.client.on('close', () => { this.log.warning("Redis connection closed") });
		this.client.on('reconnecting', (ms:number) => { this.log.info("Redis reconnecting in %d", ms) });

	}
	
}

class PromiseError extends Error {
	command:{name:string, args:string[]}
}