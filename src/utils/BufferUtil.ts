export class BufferUtil {

  bufferToRedisString(buf:Buffer):string {

    let ret = "";
    for(let byte of buf)
      ret += `\\x${byte.toString(16)}`;
    
    return `"${ret}"`
  }

  redisStringToBuffer(redisStr:string):Buffer {

    // we remove all leading and trailing ""
    redisStr = redisStr
      .replace(/^"/g, '')
      .replace(/"$/g, '')
      .replace(/\\x/g, '');

    return Buffer.from(redisStr, 'hex');
  }
}