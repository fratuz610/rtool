export class KeyTransformer {

    constructor(private pattern:string, 
      private transform:string) {
    }
    
    public mapKey(key:string):string {

      if(this.pattern === undefined || this.transform === undefined)
          return key;

      return key.replace(this.pattern, this.transform);
    }
}