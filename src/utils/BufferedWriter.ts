import * as fs from 'fs';

export class BufferedWriter {

    private pendingList:string[] = [];

    constructor(private outFile:string) {

        // we try and delete the current file
        if(fs.existsSync(this.outFile))
            fs.unlinkSync(this.outFile);
    }

    public write(data:string) {

        this.pendingList.push(data);

        if(this.pendingList.length === 10000) {

            let copyList = this.pendingList.concat();
            this.pendingList = [];

            return this.doWrite(copyList);
        }
    }

    private doWrite(writeList:string[]) {
        fs.appendFileSync(this.outFile, writeList.join(''));
    }

    public close() {
        if(this.pendingList.length > 0)
            this.doWrite(this.pendingList);
    }

}