import * as util from 'util';
import * as stream from 'stream';

enum LogLevel {
    debug = 0,
    info,
    warning,
    error
}

interface LogItem {
    t: number,
    l: string,
    md: string,
    m: string
}

interface LogStream {
    stream: stream.Writable;
    minLevel: LogLevel;
}

export class Log {

    constructor(private module:string, 
        private minLevel:LogLevel,
        private streamList:LogStream[]) {

    }
    
    debug(...args:any[]) :void { this.output(LogLevel.debug, util.format.apply(null, arguments)); }
    info(...args:any[]):void { this.output(LogLevel.info, util.format.apply(null, arguments)); }
    warning(...args:any[]):void { this.output(LogLevel.warning, util.format.apply(null, arguments)); }
    error(...args:any[]): void { this.output(LogLevel.error, util.format.apply(null, arguments)); }

    private output(logLevel:LogLevel, message:string) :void {

        if(logLevel < this.minLevel)
            return;

        var logObj = {
            t: new Date().getTime(),
            l: LogLevel[logLevel].toLocaleUpperCase(),
            md: this.module,
            m: message
        }

        // we send to all registered streams
        for(let streamObj of this.streamList)
            if(logLevel >= streamObj.minLevel)
                streamObj.stream.write(logObj);

    }
}

export class LogFactory {

    private streamList:LogStream[];

    constructor(private minLevel:LogLevel = LogLevel.info) {

        this.streamList = [{
            stream: new DefaultSteam(),
            minLevel: minLevel
        }];
    }

    get(module:string):Log {
        return new Log(module, this.minLevel, this.streamList);
    }

    clearStreams():void {
        this.streamList = [];
    }

    addStream(stream: stream.Writable, minLevel:LogLevel):void {
        this.streamList.push({
            stream: stream,
            minLevel: minLevel
        })
    }

    setMinLevel(minLevel:LogLevel) {
        this.minLevel = minLevel;
    }
}

class DefaultSteam extends stream.Writable {

    constructor() {
        super({
            highWaterMark: 16384,
            decodeStrings: false,
            objectMode : true
        })
    }

    _write(data: any, encoding: string, callback: (error?:Error) => void) {
        process.stdout.write(this.format(data));
        callback();
    }

    format(data:LogItem) {
		if(data.md && data.md !== "")
			return new Date(data.t).toISOString() + ": " + data.l + ": " + data.md + ": " + data.m + "\n";
		else
			return new Date(data.t).toISOString() + ": " + data.l + ": " + data.m + "\n";
	}
}