import { ArraySplitter } from "./ArraySplitter";

let customList = [1,2,3,4,5,6,7,8,9,10]

test('it can split an array correctly - happy path', () => {

    let arraySplitter = new ArraySplitter(customList, 3);
    expect(arraySplitter.nextChunk()).toEqual([1,2,3]);
    expect(arraySplitter.nextChunk()).toEqual([4,5,6]);
    expect(arraySplitter.nextChunk()).toEqual([7,8,9]);
    expect(arraySplitter.nextChunk()).toEqual([10]);
    expect(arraySplitter.nextChunk()).toEqual([]);
})

test('it handles small arrays correctly', () => {

    let arraySplitter = new ArraySplitter(customList, 20);
    expect(arraySplitter.nextChunk()).toEqual(customList);
    expect(arraySplitter.nextChunk()).toEqual([]);
})