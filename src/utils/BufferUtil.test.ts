import { BufferUtil } from "./BufferUtil";

let bufferUtil = new BufferUtil();

describe('bufferUtil', () => {

  it('can convert a buffer to redis hex string successfully', () => {

    let buf = Buffer.from('hello world', 'ascii');
    let redisStr = bufferUtil.bufferToRedisString(buf);

    expect(redisStr).toEqual('"\\x68\\x65\\x6c\\x6c\\x6f\\x20\\x77\\x6f\\x72\\x6c\\x64"');
  })

  it('can convert a redis hex string to a buffer successfully', () => {
    
    let redisStr = '"\\x68\\x65\\x6c\\x6c\\x6f\\x20\\x77\\x6f\\x72\\x6c\\x64"';

    let buf = bufferUtil.redisStringToBuffer(redisStr);

    expect(Buffer.from('hello world', 'ascii').compare(buf) === 0);
  })

})