export class ArraySplitter {

    curChunk = 0;

    constructor(private list:any[], 
        private chunkSize:number) {

    }

    nextChunk():any[] {
        let cur = this.curChunk++*this.chunkSize;
        return this.list.slice(cur, cur + this.chunkSize);
    }
    
}