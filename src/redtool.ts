import * as yargs from 'yargs'
import { ExportCmd } from "./export/ExportCmd";
import { ImportCmd } from './import/ImportCmd';
import { SizeCmd } from './size/SizeCmd';

let exportCmd = new ExportCmd();
let importCmd = new ImportCmd();
let sizeCmd = new SizeCmd();

let argv = yargs
    .usage("Usage: $0 <command> [options]")
    .command(exportCmd.commandModule)
    .command(importCmd.commandModule)
    .command(sizeCmd.commandModule)
    .example("export nnn:dev testFile.rtool", 'exports the whole nnn:dev keyset to testFile.txt')
    .example("export nnn:dev testFile.rtool", 'exports the whole nnn:dev keyset to testFile.txt transforming every occcurrence of nnn:dev into nnn:prod')
    .example("import testFile.rtool", 'imports a previously exported testFile.txt into Redis')
    .example("size", 'evaluates the size of the whole database - same as `size *`')
    .example("size *", 'evaluates the size of the whole database')
    .example("size something*", 'evaluates the size of the keys starting with `something`')
    .example("size specific-key", 'evaluates the size of a specific key`')
    
    .help('help')
    
    .demandCommand()

    .alias('h', 'host')
    .alias('p', 'port')
    .default('host', "127.0.0.1")
    .default('port', 6379)
    .argv;
