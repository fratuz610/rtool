import { CommandModule, Argv } from "yargs";

import * as IoRedis from 'ioredis';
import * as fs from 'fs';
import * as readline from 'readline';

import { ImportConfig } from "../model/Model";

export class ImportCmd {

    private client:IoRedis.Redis;
    private importStats:ImportStats;

    constructor() {

        this.importStats = new ImportStats();
    }

    private importHandler(argv:ImportConfig) {

        this.doImport(argv)
            .then(() => {
                console.log("All done");
                process.exit(0);
            })
            .catch(err => {
                console.log("ERROR: %s", err);
                process.exit(-1);
            });
    }

    private async doImport(argv:ImportConfig):Promise<void> {

        console.log("Connecting to %s:%d", argv.host, argv.port);

        this.client = new IoRedis(argv.port, argv.host, { lazyConnect: true });
        this.client.on('error', (err:Error) => { 
            console.log("Redis error: %s -> %s", err.name, err.message) 
            this.client.quit();
        });

        // we connect
        await this.client.connect();

        console.log("...connected");

        console.log("Opening file `%s`", argv.input);

        // we open the file
        let readStream = fs.createReadStream(argv.input, {encoding:'utf8'});
        
        // we wait until the stream is readable
        await this.processFile(readStream, argv.dryRun);

        console.log("Lines read: %d", this.importStats.lineRead);

        readStream.close();

        // we disconnect
        await this.client.disconnect();

        return Promise.resolve();
    }

    private processFile(inStream:fs.ReadStream, dryRun:boolean):Promise<void> {

      let rl = readline.createInterface(inStream);

      let lineList = [];

      return new Promise<void>((resolve, reject) => {
        rl.on('line', async (line) => {
            
          //console.log(line);

          lineList.push(line);
          
          if(lineList.length === 1000) {

            let processList = lineList.concat();
            lineList = [];
            
            try {
              await this.processChunk(processList, dryRun);
            } catch(err) {
              return reject(err);
            }
              
          }
        })
        rl.on('error', (err) => {
          return reject(err);
        })
        rl.on('close', async () => {

          if(lineList.length > 0)
            await this.processChunk(lineList, dryRun);
          
          console.log("Import stats:\n-> line processed: %d\n-> keys processed: %d", this.importStats.lineRead, this.importStats.keysRead.size);

          return resolve();
        })
      });
        
    }

    private async processChunk(lineList:string[], dryRun:boolean):Promise<void> {
        
      this.importStats.lineRead += lineList.length;

      let pipeline:IoRedis.Pipeline;

      if(!dryRun)
        pipeline = this.client.pipeline();

      for(let line of lineList) {
          
        let lineList = line.split(' ');

        if(lineList.length < 2)
          return Promise.reject(new Error("Malformed line: `" + line + "`"));

        // we decode all tokens
        let decodedList = lineList.map(token => decodeURI(token));
        
        let op = decodedList[0];

        if(op.toLowerCase() !== 'del')
          this.importStats.keysRead.add(decodedList[1]);
        
        if(dryRun)
          continue;

        //@ts-ignore
        pipeline.call(...decodedList);
      }

      if(!dryRun) {
        let replies = await pipeline.exec();

        for(let reply of replies) {

          if(reply[0])
            return Promise.reject(reply[0]);
        }
      }
      
      return Promise.resolve();
    }

    public commandModule:CommandModule = {
        command: 'import <input>',
        aliases: 'i',
        describe: 'Imports redis data from an exported file',
        builder: {
            'input': {
                alias: 'i',
                type: 'string' as 'string',
                demand: 'Please specify a input file',
                nargs: 1,
                describe: 'Specifies a input file',
            }, 
            'dryRun': {
                alias: 'd',
                type: 'boolean',
                nargs: 0,
                describe: 'Allows for a dry run',
            }
        },
        handler: (argv) => { this.importHandler(argv); }
    }

}

class ImportStats {
    lineRead:number = 0;
    keysRead:Set<string> = new Set();
    stringCount:number = 0;
    hashCount:number = 0;
    setCount:number = 0;
    listCount:number = 0;
    zsetCount:number = 0;
}
