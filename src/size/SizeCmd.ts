import { CommandModule, Argv } from "yargs";
import { SizeConfig } from '../model/Model';
import * as IoRedis from 'ioredis';
import * as prettyBytes from 'pretty-bytes';

export class SizeCmd {

  private client:IoRedis.Redis;

  constructor() {

  }

  private sizeHandler(argv:SizeConfig) {

    this.calculateSize(argv)
      .then(() => {
        console.log("All done");
        process.exit(0);
      })
      .catch(err => {
        console.log("ERROR: %s", err);
        process.exit(-1);
      });
  }

  private async calculateSize(argv:SizeConfig):Promise<void> {

    console.log(`Connecting to ${argv.host}:${argv.port}...`);

    this.client = new IoRedis(argv.port, argv.host, { lazyConnect: true });
    this.client.on('error', (err:Error) => { 
      console.log(`Redis error: ${err.name} -> ${err.message}`) 
      this.client.quit();
    });

    // we connect
    await this.client.connect();

    console.log(`=> connection established.`);

    console.log(`Evaluating path '${argv.path}'`);

    // we scan through the keys

    let replyList = [];
    let cursor = 0;
    let bytesTotal = 0;
    let iteration = 0;
    let totalKeys = 0;

    while(true) {

      // we query adding the dot (.) back
      let scanReply = await this.client.scan(cursor, "MATCH", argv.path, "COUNT", 1000);

      cursor = parseInt(scanReply[0]);
      let keyList = scanReply[1];

      totalKeys += keyList.length;
      iteration++;

      if(iteration % 10 === 0)
        console.log(`=> iteration: ${iteration} / key evaluated: ${totalKeys}`);

      let sizePipeline = this.client.pipeline();

      for(let key of keyList)
        sizePipeline.memory('USAGE', key);
      
      let replies = await sizePipeline.exec();

      for(let reply of replies) {

        if(reply[0])
          return Promise.reject(reply[0]);
        
        bytesTotal += parseInt(reply[1]);
      }

      if(cursor === 0)
        break;
    }

    // for each bunch of keys we invoce MEMORY USAGE on the key
    // we add back to the total
    console.log(`Size: ${prettyBytes(bytesTotal)} across ${totalKeys} key(s)`);
    
    // we disconnect
    this.client.disconnect();

    return Promise.resolve();
  }

  public commandModule:CommandModule = {
    command: 'size [path]',
    aliases: 's',
    describe: 'Estimate the size of all keys matching the specified path or *',
    builder: {
      'path': {
        type: 'string' as 'string',
        default: '*',
        nargs: 0,
        describe: 'Specifies the path expression',
      }
    },
    handler: (argv) => { this.sizeHandler(argv); }
  }

}