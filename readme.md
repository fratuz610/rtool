# redtool

A swiss army knife for Redis data import/export.

- Based on the amazing [ioredis](https://www.npmjs.com/package/ioredis) node.js lib
- Heavy use of pipelines and multi inserts for gentle exports and imports
- Byte safe exports (hex encoded via [encodeURI()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURI))

# Installation

`npm install -g redtool`

# Export

`redtool export <pattern> <file.redtool>`

or

`redtool export <pattern> <file.redtool> -f <filter>`

Example 1:
```
redtool export * export-all.rtool
```

Example 2:
```
redtool export something:prod:* export-something-transform.rtool -f something:dev
```

# Import

Redtool export files can be imported using Redtool itself

`redtool import <file.rtool>`

i.e.
```
redtool import export-all.redtool
```

or using the redis-cli

`cat <file.rtool> | redis-cli --pipe`

i.e.
```
cat export-all.redtool | redis-cli --pipe
```

Please note that imports via redtool are broken down into multiple pipeline transactions and hence more gentle than via the `redis-cli` which commits all data in one big transaction.

# Size

Helps evaluating the size of the database or parts of it

`redtool size [path]`

Whole database: 
```
redtool size 
```

All keyspace starting with `something`: 
```
redtool size something*
```

Only a specific key size
```
redtool size specific-key
```


# Help

```
> redtool --help

Usage: redtool <command> [options]

Commands:
  redtool export <filter> <output>  Exports a subset of a redis keyspace to a
                                    file                            [aliases: e]
  redtool import <input>            Imports redis data from an exported file
                                                                    [aliases: i]
  redtool size [path]               Estimate the size of all keys matching the
                                    specified path or *             [aliases: s]

Options:
  --version   Show version number                                      [boolean]
  --help      Show help                                                [boolean]
  -h, --host                                              [default: "127.0.0.1"]
  -p, --port                                                     [default: 6379]

Examples:
  export nnn:dev testFile.rtool  exports the whole nnn:dev keyset to
                                 testFile.txt
  export nnn:dev testFile.rtool  exports the whole nnn:dev keyset to
                                 testFile.txt transforming every occcurrence of
                                 nnn:dev into nnn:prod
  import testFile.rtool          imports a previously exported testFile.txt into
                                 Redis
  size *                         
  size *                  
  size something:*               evaluates the size of the keys starting with
                                 `something:`

```
